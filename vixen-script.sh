#!bash

apt update
apt install git npm nodejs make gcc pkg-config 
sudo apt-get install build-essential
sudo apt-get install libx11-dev libxkbfile-dev 

#install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt update && apt install yarn


yarn

echo "deb [trusted=yes] https://apt.fury.io/caddy/ /" \
    | sudo tee -a /etc/apt/sources.list.d/caddy-fury.list

apt update && apt install caddy

caddy start

############

apt install apache2-utils


yarn start /my-workspace --hostname 0.0.0.0 --port 8080
